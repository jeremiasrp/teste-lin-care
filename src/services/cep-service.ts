import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class CepServiceProvider {


    url = 'https://viacep.com.br/ws/';    

    constructor(public http: Http) {

    }

    getCep(cepNumber:string): Promise<any[]> {
        return new Promise(resolve => {
            this.http.get(this.url + cepNumber + '/json')
                .toPromise()
                .then(
                resposta => {
                    let dados = resposta.json();
                    let endereco = [];
                    endereco.push(
                        {
                            bairro: dados.bairro,
                            cep: dados.cep,
                            complemento: dados.complemento,
                            gia: dados.gia,
                            ibge: dados.ibge,
                            localidade: dados.localidade,
                            logradouro: dados.logradouro,
                            uf: dados.uf,
                            unidade: dados.unidade
                        });
                    resolve(endereco);
                });
        });
    }

}


