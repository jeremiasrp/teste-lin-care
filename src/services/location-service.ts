import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LocationServiceProvider {


    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

    constructor(public http: Http) {

    }

    getMapLatlng(adress: string): Promise<any[]> {
        return new Promise(resolve => {
            this.http.get(this.url + adress + '&key=AIzaSyAs6QvR2sTKhKlPt2iuLyphjNin8hgOsr4')
                .toPromise()
                .then(
                resposta => {
                    let dados = resposta.json();
                    let coordinate = [];
                    coordinate.push(
                        {
                            lat: dados.results[0].geometry.location.lat,
                            lng: dados.results[0].geometry.location.lng
                        });
                    resolve(coordinate);
                });
        });
    }

}