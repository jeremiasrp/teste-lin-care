import { Component, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { CepServiceProvider } from '../services/cep-service';
import { LocationServiceProvider } from '../services/location-service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  cepNumber:string = "";
  adress: any[];
  adress_search: string = "";
  show_adress: string = "";
  location_lat: string;
  location_lng: string;


  constructor(public cepService: CepServiceProvider, public locationService: LocationServiceProvider) {}

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  ngOnInit() {
    var mapProp = {
      center: new google.maps.LatLng(-19.864168, -43.953435),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

  searchCep(){
    this.cepService.getCep(this.cepNumber).then(dados => {
      this.adress = dados;
      this.adress_search = this.adress[0].logradouro + "+"  + this.adress[0].bairro + "+" + this.adress[0].localidade + "+" + this.adress[0].uf;
      this.adress_search = this.adress_search.replace(/\s/g,'');
      this.show_adress = this.adress[0].logradouro + ", "  + this.adress[0].bairro + ", " + this.adress[0].localidade + ", " + this.adress[0].uf;
      this.addLocationMark();
    });
    
  } 

  addLocationMark() {
    this.locationService.getMapLatlng(this.adress_search).then(local => {
      this.location_lat = local[0].lat;
      this.location_lng = local[0].lng;
    });

    var mapProp = {
      center: new google.maps.LatLng(Number.parseFloat(this.location_lat), Number.parseFloat(this.location_lng)),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);


    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(Number.parseFloat(this.location_lat), Number.parseFloat(this.location_lng)),
      map: this.map
    });
  }
}
