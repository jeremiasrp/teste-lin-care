import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CepServiceProvider } from '../services/cep-service';
import { LocationServiceProvider } from '../services/location-service'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    CepServiceProvider,
    LocationServiceProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
